//
//  PurchaseViewController.swift
//  nyxl
//
//  Created by Martin Burolla on 12/21/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import UIKit

class PurchaseViewController: UIViewController, UIWebViewDelegate {

    // MARK: - Data members
    
    let webView = UIWebView()
    let amazonURL = "http://www.amazon.com/gp/product/B00IZCSX9U?keywords=nyxl%201046&qid=1450825063&ref_=sr_1_1&sr=8-1"
    let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    var didFinishedLoadingCount = 0
    var didStartLoadingCount = 0
    
    // MARK: - Lifecycle
 
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.setupUI()
        self.setupAutolayout()
    }
    
    // MARK: - UIWebViewDelegate
    
    func webViewDidStartLoad(webView: UIWebView) {
        self.didStartLoadingCount += 1
        
        if(self.didStartLoadingCount == 1) {
            NSOperationQueue.mainQueue() .addOperationWithBlock { () -> Void in
                self.activityIndicatorView.hidden = false
                self.activityIndicatorView.startAnimating()
            }
        }
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.didFinishedLoadingCount += 1
        
        if (self.didFinishedLoadingCount == 1){
            NSOperationQueue.mainQueue() .addOperationWithBlock { () -> Void in
                self.activityIndicatorView.hidden = true
                self.activityIndicatorView.stopAnimating()
            }
        }
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        // TODO: Handle error.
    }
    
    // MARK: - UI
    
    private func setupUI() {
        
        // Webview.
        self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.webView.delegate = self
        let request = NSURLRequest(URL: NSURL(string: amazonURL)!)
        self.webView.loadRequest(request)
        self.view.addSubview(self.webView)
        
        // Activity indicator.
        self.activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        self.activityIndicatorView.backgroundColor = NYXLStyleSheet.extraLightGrayColor
        self.view.addSubview(self.activityIndicatorView)
    }
    
    private func setupAutolayout() {
        
        // Webview.
        self.webView.topAnchor.constraintEqualToAnchor(self.view.topAnchor, constant: 24).active = true;
        self.webView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor).active = true;
        self.webView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor).active = true;
        self.webView.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor).active = true;
        
        // Activity indicator.
        self.activityIndicatorView.topAnchor.constraintEqualToAnchor(self.view.topAnchor).active = true;
        self.activityIndicatorView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor).active = true;
        self.activityIndicatorView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor).active = true;
        self.activityIndicatorView.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor).active = true;
    }
}
