//
//  DataSourceProtocols.swift
//  nyxl
//
//  Created by Martin Burolla on 1/18/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation

// MARK: - Local Storage

protocol LocalSettingsProxyProtocol {
    
    // Last background fetch date.
    func saveDateOfLastBackgroundFetch(date: NSDate)
    func getDateOfLastBackgroundFetch() -> NSDate
}
