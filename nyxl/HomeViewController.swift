//
//  ViewController.swift
//  nyxl
//
//  Created by Martin Burolla on 12/19/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController {

    // MARK: - Data members

    var buttons = [UIButton]()
    let kButtonHeight: CGFloat = 60.0
    let kButtonSpacing: CGFloat = 30.0
    let kButtonPadding: CGFloat = 20.0
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupAutolayout()
    }
    
    // MARK: - Button Actions
    
    func buttonLowEStringTouched(sender:UIButton!) {
        SoundManager.playSoundForString(GuitarStrings.LowEString)
    }
    
    func buttonAStringTouched(sender:UIButton!) {
        SoundManager.playSoundForString(GuitarStrings.AString)
    }
    
    func buttonDStringTouched(sender:UIButton!) {
        SoundManager.playSoundForString(GuitarStrings.DString)
    }
    
    func buttonGStringTouched(sender:UIButton!) {
        SoundManager.playSoundForString(GuitarStrings.GString)
    }
    
    func buttonBStringTouched(sender:UIButton!) {
        SoundManager.playSoundForString(GuitarStrings.BString)
    }
    
    func buttonHighEStringTouched(sender:UIButton!) {
        SoundManager.playSoundForString(GuitarStrings.HighEString)
    }
    
    // MARK: - Private helpers
    
    private func createButton(selectorName: String, stringColor: UIColor, title: String) -> UIButton {
        let retval = UIButton(type: .System)
        retval.enabled = true
        retval.backgroundColor = stringColor
        retval.setTitle(title, forState: .Normal)
        retval.tintColor = UIColor.blackColor()
        retval.titleLabel?.font = NYXLStyleSheet.guitarStringLabelFont
        retval.addTarget(self, action: Selector(selectorName), forControlEvents: UIControlEvents.TouchUpInside)
        retval.translatesAutoresizingMaskIntoConstraints = false
        return retval
    }
    
    // MARK: - UI
    
    func setupUI() {
       view.backgroundColor = NYXLStyleSheet.extraLightGrayColor
       
       buttons.append(self.createButton("buttonLowEStringTouched:", stringColor: UIColor.yellowColor(), title:"Low E String"))
       view.addSubview(self.buttons[GuitarStrings.LowEString.hashValue])
       
       buttons.append(self.createButton("buttonAStringTouched:", stringColor: UIColor.redColor(), title: "A String"))
       view.addSubview(self.buttons[GuitarStrings.AString.hashValue])
       
       buttons.append(self.createButton("buttonDStringTouched:", stringColor: UIColor.darkGrayColor(), title: "D String"))
       view.addSubview(self.buttons[GuitarStrings.DString.hashValue])
       
       buttons.append(self.createButton("buttonGStringTouched:", stringColor: UIColor.greenColor(), title: "G String"))
       view.addSubview(self.buttons[GuitarStrings.GString.hashValue])
       
       buttons.append(self.createButton("buttonBStringTouched:", stringColor: UIColor.purpleColor(), title: "B String"))
       view.addSubview(self.buttons[GuitarStrings.BString.hashValue])
       
       buttons.append(self.createButton("buttonHighEStringTouched:", stringColor: UIColor.lightGrayColor(), title: "High E String"))
       view.addSubview(self.buttons[GuitarStrings.HighEString.hashValue])
    }
    
    func setupAutolayout() {
       buttons[GuitarStrings.HighEString.hashValue].topAnchor.constraintEqualToAnchor(self.view.topAnchor, constant: 50).active = true
       buttons[GuitarStrings.HighEString.hashValue].leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant: kButtonPadding).active = true
       buttons[GuitarStrings.HighEString.hashValue].trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -kButtonPadding).active = true
       buttons[GuitarStrings.HighEString.hashValue].heightAnchor.constraintEqualToConstant(kButtonHeight).active = true
       
       buttons[GuitarStrings.BString.hashValue].topAnchor.constraintEqualToAnchor(self.buttons[GuitarStrings.HighEString.hashValue].bottomAnchor, constant: kButtonSpacing).active = true
       buttons[GuitarStrings.BString.hashValue].leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant: kButtonPadding).active = true
       buttons[GuitarStrings.BString.hashValue].trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -kButtonPadding).active = true
       buttons[GuitarStrings.BString.hashValue].heightAnchor.constraintEqualToConstant(kButtonHeight).active = true
       
       buttons[GuitarStrings.GString.hashValue].topAnchor.constraintEqualToAnchor(self.buttons[GuitarStrings.BString.hashValue].bottomAnchor, constant: kButtonSpacing).active = true
       buttons[GuitarStrings.GString.hashValue].leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant: kButtonPadding).active = true
       buttons[GuitarStrings.GString.hashValue].trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -kButtonPadding).active = true
       buttons[GuitarStrings.GString.hashValue].heightAnchor.constraintEqualToConstant(kButtonHeight).active = true
       
       buttons[GuitarStrings.DString.hashValue].topAnchor.constraintEqualToAnchor(self.buttons[GuitarStrings.GString.hashValue].bottomAnchor, constant: kButtonSpacing).active = true
       buttons[GuitarStrings.DString.hashValue].leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant: kButtonPadding).active = true
       buttons[GuitarStrings.DString.hashValue].trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -kButtonPadding).active = true
       buttons[GuitarStrings.DString.hashValue].heightAnchor.constraintEqualToConstant(kButtonHeight).active = true
       
       buttons[GuitarStrings.AString.hashValue].topAnchor.constraintEqualToAnchor(self.buttons[GuitarStrings.DString.hashValue].bottomAnchor, constant: kButtonSpacing).active = true
       buttons[GuitarStrings.AString.hashValue].leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant: kButtonPadding).active = true
       buttons[GuitarStrings.AString.hashValue].trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -kButtonPadding).active = true
       buttons[GuitarStrings.AString.hashValue].heightAnchor.constraintEqualToConstant(kButtonHeight).active = true
    
       buttons[GuitarStrings.LowEString.hashValue].topAnchor.constraintEqualToAnchor(self.buttons[GuitarStrings.AString.hashValue].bottomAnchor, constant: kButtonSpacing).active = true
       buttons[GuitarStrings.LowEString.hashValue].leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant: kButtonPadding).active = true
       buttons[GuitarStrings.LowEString.hashValue].trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -kButtonPadding).active = true
       buttons[GuitarStrings.LowEString.hashValue].heightAnchor.constraintEqualToConstant(kButtonHeight).active = true
    }
}
