//
//  GuitarStringsEnum.swift
//  nyxl
//
//  Created by Martin Burolla on 12/19/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import Foundation

enum GuitarStrings {
    case LowEString, AString, DString, GString, BString, HighEString
    
    var description : String {
        switch self {
        case .LowEString: return "LowEString"
        case .AString: return "AString"
        case .DString: return "DString"
        case .GString: return "GString"
        case .BString: return "BString"
        case .HighEString: return "HighEString"
        }
    }
    
    static let allValues = [LowEString, AString, DString, GString, BString, HighEString]
}

enum iCloudType {
    case Private, Public
}