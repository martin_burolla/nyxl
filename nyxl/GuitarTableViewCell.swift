//
//  GuitarTableViewCell.swift
//  nyxl
//
//  Created by Martin Burolla on 12/23/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import UIKit

class GuitarTableViewCell: UITableViewCell {

    // MARK: - Data members
    
    var guitarNameLabel        = UILabel()
    var stringDateChangedLabel = UILabel()
    var stringAgeInDaysLabel   = UILabel()
    var needsStringChange      = false
    
    // MARK: - Construction
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, guitarViewModel: GuitarViewModel) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        guitarNameLabel.text        = guitarViewModel.model
        stringDateChangedLabel.text = guitarViewModel.formattedDateChanged
        stringAgeInDaysLabel.text   = "\(guitarViewModel.daysSinceLastChanged)"
        needsStringChange           = guitarViewModel.needsStringChange
        
        setupUI()
        setupAutolayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: -  UI
    
    func setupUI() {
        
        // Guitar model
        guitarNameLabel.backgroundColor = UIColor.whiteColor()
        guitarNameLabel.translatesAutoresizingMaskIntoConstraints = false
        guitarNameLabel.font = NYXLStyleSheet.guitarNameFont
        contentView.addSubview((self.guitarNameLabel))
        
        // Guitar date changed label.
        stringDateChangedLabel.backgroundColor = UIColor.whiteColor()
        stringDateChangedLabel.translatesAutoresizingMaskIntoConstraints = false
        stringDateChangedLabel.font = NYXLStyleSheet.dateChangedFont
        contentView.addSubview((self.stringDateChangedLabel))

        
        // Guitar string age label.
        stringAgeInDaysLabel.backgroundColor = UIColor.whiteColor()
        stringAgeInDaysLabel.translatesAutoresizingMaskIntoConstraints = false;
        stringAgeInDaysLabel.font = NYXLStyleSheet.guitarStringAgeFont
        if needsStringChange {
            self.stringAgeInDaysLabel.textColor = UIColor.redColor()
        }
        
        self.contentView.addSubview((self.stringAgeInDaysLabel))
    }
    
    // MARK: - AutoLayout
    
    func setupAutolayout() {

        // Guitar model.
        guitarNameLabel.topAnchor.constraintEqualToAnchor(self.topAnchor, constant:  3).active = true
        guitarNameLabel.leadingAnchor.constraintEqualToAnchor(self.leadingAnchor, constant:  5).active = true
        guitarNameLabel.trailingAnchor.constraintEqualToAnchor(self.trailingAnchor, constant: NYXLStyleSheet.trailingDivider).active = true
        guitarNameLabel.heightAnchor.constraintEqualToConstant(NYXLStyleSheet.guitarNameLabelHeight).active = true
        
        // Guitar date changed label.
        stringDateChangedLabel.topAnchor.constraintEqualToAnchor(self.guitarNameLabel.bottomAnchor).active = true
        stringDateChangedLabel.leadingAnchor.constraintEqualToAnchor(self.leadingAnchor,constant:  5).active = true
        stringDateChangedLabel.trailingAnchor.constraintEqualToAnchor(self.trailingAnchor, constant: NYXLStyleSheet.trailingDivider).active = true
        stringDateChangedLabel.heightAnchor.constraintEqualToConstant(NYXLStyleSheet.stringDateChangedLabelHeight).active = true
        
        // String age label.
        stringAgeInDaysLabel.topAnchor.constraintEqualToAnchor(self.topAnchor, constant:  6).active = true
        stringAgeInDaysLabel.leadingAnchor.constraintEqualToAnchor(self.guitarNameLabel.trailingAnchor).active = true
        stringAgeInDaysLabel.trailingAnchor.constraintEqualToAnchor(self.trailingAnchor).active = true
        stringAgeInDaysLabel.heightAnchor.constraintEqualToConstant(NYXLStyleSheet.stringAgeInDaysLabelHeight)
    }
}
