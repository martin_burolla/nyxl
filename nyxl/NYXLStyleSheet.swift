//
//  NYXLStyleSheet.swift
//  nyxl
//
//  Created by Martin Burolla on 12/28/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import UIKit


public class NYXLStyleSheet { // http://iosfonts.com/
    
    // Home tab
    static let guitarStringLabelFont = UIFont(name: "Arial-BoldMT", size: 20.0)!
    
    // Guitars tab
    static let dateChangedFont = UIFont(name: "Arial-ItalicMT", size: 11.0)!
    static let guitarNameFont = UIFont(name: "Arial-BoldMT", size: 20.0)!
    static let guitarStringAgeFont = UIFont(name: "Arial-BoldMT", size: 25.0)!
    static let trailingDivider: CGFloat = -60.0
    static let guitarNameLabelHeight: CGFloat = 20
    static let stringDateChangedLabelHeight: CGFloat = 20
    static let stringAgeInDaysLabelHeight: CGFloat = 60
    static let addGuitarButtonHeight: CGFloat = 50
    
    // Settings tab
    static let pickerViewHeight: CGFloat = 200
    
    // Common
    static let extraLightGrayColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
    static let statusBarHeight: CGFloat = 24.0
}