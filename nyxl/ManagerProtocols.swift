//
//  ManagerProtocols.swift
//  nyxl
//
//  Created by Martin Burolla on 1/18/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation

// MARK: - Guitar Management

protocol GuitarManagerProtocol {
    var delegate: GuitarManagerDelegate! {get set}
    
    func fetchAllGuitars()
    func addGuitar(newGuitar: GuitarViewModel)
    func deleteGuitar(guitar: GuitarViewModel)
    func updateDateForGuitar(guitar: GuitarViewModel, newDate: NSDate)
}

protocol GuitarManagerDelegate {
    func didFetchAllGuitars(guitarManager: GuitarManagerProtocol, guitars: Array<GuitarViewModel>, error: NSError?)
    func didAddNewGuitar(guitarManager: GuitarManagerProtocol, guitar: GuitarViewModel, error: NSError?)
    func didDeleteGuitar(guitarManager: GuitarManagerProtocol, guitar: GuitarViewModel, error: NSError?)
    func didUpdateDateForGuitar(guitarManager: GuitarManagerProtocol, guitar: GuitarViewModel, error: NSError?)
}

// MARK: - Notification

protocol GuitarNotificationManagerProtocol {
    func showLocalNotificationForGuitarsThatNeedNewStrings()
    func shouldRunBackgroundFetchToday() -> Bool
    static func setupBackgroundFetchForDailyRefreshInterval()
    static func askUserForPermisionToShowLocalAlerts()
}

// MARK: - Sound

protocol SoundManagerProtocol {
    static func playSoundForString(theString: GuitarStrings)
}

// MARK: - User Settings

protocol UserSettingsProtocol {
    func saveDateOfLastBackgroundFetch(date: NSDate)
    func getDateOfLastBackgroundFetch() -> NSDate
}

protocol UserSettingsDelegate {
    func didFetchUserSettings(userSettingsManager: UserSettingsProtocol, userSettings: UserSetting?, error: NSError?)
    func didUpdateUserSettings(userSettingsManager: UserSettingsProtocol, userSettings: UserSetting, error: NSError?)
}