//
//  GuitarNotificationManager.swift
//  nyxl
//
//  Created by Martin Burolla on 1/16/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit


// MARK: - GuitarNotificationManager

final class GuitarNotificationManager: NSObject, GuitarManagerDelegate, GuitarNotificationManagerProtocol {

    // MARK: - Data Members
    
    let guitarManager       = GuitarManager()
    let userSettingsManager = UserSettingsManager()
    let NumSecondsInOneDay  = 86400.0
    
    // MARK: - Construction
    
    override init() {
        super.init()
        guitarManager.delegate = self
    }
    
    // MARK: - Background Fetch
    
    static func setupBackgroundFetchForDailyRefreshInterval() {
        UIApplication.sharedApplication().setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
    }
    
    // MARK: - Notification
    
    func showLocalNotificationForGuitarsThatNeedNewStrings() {
        if shouldRunBackgroundFetchToday() {
            guitarManager.fetchAllGuitars()
        }
    }
    
    func shouldRunBackgroundFetchToday() -> Bool {
        var retval: Bool = false
        let dateLastRan = userSettingsManager.getDateOfLastBackgroundFetch()
        let deltaSeconds = NSDate().timeIntervalSince1970 - dateLastRan.timeIntervalSince1970
        
        if deltaSeconds > NumSecondsInOneDay {
            retval = true
            userSettingsManager.saveDateOfLastBackgroundFetch(NSDate())
        }
        return retval
    }
    
    private func showUILocalNotificationAlert(sender: AnyObject, numGuitars: Int) {
        let settings = UIApplication.sharedApplication().currentUserNotificationSettings()
        
        if settings!.types == .None {
            print("Need to call GuitarNotificationManager.askUserForPermisionToShowLocalAlerts().")
        }
        else {
            let notification = UILocalNotification()
            notification.fireDate = NSDate(timeIntervalSinceNow: 1)
            if numGuitars == 1 {
                notification.alertBody = "You have \(numGuitars) guitar that needs new strings!"
            } else {
               notification.alertBody = "You have \(numGuitars) guitars that need new strings!"
            }
            notification.alertAction = "OK"
            notification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.sharedApplication().scheduleLocalNotification(notification)
        }
    }
    
    static func askUserForPermisionToShowLocalAlerts() {
        let notificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
    }
    
    // MARK: - CloudKitProxyDelegate
    
    func didFetchAllGuitars(guitarManager: GuitarManagerProtocol, guitars: Array<GuitarViewModel>, error: NSError?) {
        let filteredGuitars = guitars.filter({ $0.needsStringChange == true } )
        if filteredGuitars.count > 0 {
            showUILocalNotificationAlert(self, numGuitars: filteredGuitars.count)
        }
    }
    
    func didAddNewGuitar(guitarManager: GuitarManagerProtocol, guitar: GuitarViewModel, error: NSError?) {
        // NO OP
    }
    
    func didDeleteGuitar(guitarManager: GuitarManagerProtocol, guitar: GuitarViewModel, error: NSError?) {
        // NO OP
    }
    
    func didUpdateDateForGuitar(guitarManager: GuitarManagerProtocol, guitar: GuitarViewModel, error: NSError?) {
        // NO OP
    }
}
