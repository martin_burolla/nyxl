//
//  Guitar.swift
//  nyxl
//
//  Created by Marty Burolla on 1/20/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation


class Guitar: NSObject,
            iCloudStorage,
            iCloudSavable,
            iCloudDeletable,
            iCloudUpdateable,
            iCloudRecallable {

    // CK  - Field Name : Field Type
    
    var ID              : String = ""   
    var Model           : String = ""
    var LastChangedDate : NSDate = NSDate()
    
    // MARK: - Construction
    
    init(ID: String) {
        self.ID = ID
    }
    
    init(ID: String, model: String, lastChangedDate: NSDate) {
        self.ID = ID
        self.Model = model
        self.LastChangedDate = lastChangedDate
    }
    
    init(guitarViewModel: GuitarViewModel) {
        self.ID = guitarViewModel.ID
        self.Model = guitarViewModel.model
        self.LastChangedDate = guitarViewModel.dateChanged
    }
}
