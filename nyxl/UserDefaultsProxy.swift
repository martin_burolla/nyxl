//
//  UserDefaultsProxy.swift
//  nyxl
//
//  Created by Marty Burolla on 1/12/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

// MARK: - UserDefaultsProxy

final class UserDefaultsProxy: NSObject, LocalSettingsProxyProtocol {
    
    // MARK: - Data Members
    
    let BackgroundFetchDateKey = "BackgroundFetchDateKey"
    let InitializationKey      = "InitializationKey"
    let defaults = NSUserDefaults.standardUserDefaults()
    
    // MARK: - Construction
    
    override init() {
        super.init()
    }
    
    // MARK: - Background Fetch
    
    func saveDateOfLastBackgroundFetch(date: NSDate) {
        defaults.setObject(date, forKey: BackgroundFetchDateKey)
    }
    
    func getDateOfLastBackgroundFetch() -> NSDate {
        return defaults.objectForKey(BackgroundFetchDateKey) as! NSDate
    }
    
    // MARK: - Initialization
    
    func saveInitializationFlag(value: Bool) {
        defaults.setObject(value, forKey: InitializationKey)
    }
    
    func getInitializationFlag() -> Bool? {
        return defaults.objectForKey(InitializationKey) as? Bool
    }
}
