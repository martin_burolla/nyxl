//
//  NotificationAlertDays.swift
//  nyxl
//
//  Created by Marty Burolla on 1/19/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

final class AlertDaysViewModel: NSObject {

    // MARK: - Data Memebers
    
    static let Days = ["5","10","15","20","25","30","35","40","45","50", "55", "60", "120", "200"]
}
