//
//  DataModelProtocols.swift
//  nyxl
//
//  Created by Marty Burolla on 1/20/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation
import CloudKit

protocol iCloudStorage {
    var ID : String { get set }
}
protocol iCloudSavable    : iCloudStorage { }
protocol iCloudDeletable  : iCloudStorage { }
protocol iCloudUpdateable : iCloudStorage { }
protocol iCloudRecallable : iCloudStorage { }

extension iCloudStorage {
    
    func convertToCKRecord() -> CKRecord {
        let mirror = Mirror(reflecting: self)
        let ID = CKRecordID(recordName: self.ID)
        let className = NSStringFromClass((self.dynamicType as? AnyClass)!).componentsSeparatedByString(".").last!
        let retval: CKRecord = CKRecord(recordType: className, recordID: ID)
        for case let (label?, value) in mirror.children.filter({$0.label != "ID"}){
            retval.setValue(value as? AnyObject, forKey: label) // CloudKit creates the correct type.
        }
        return retval
    }
    
    // TODO: Determine how to build a data model object from a CKRecord.
    func convertFromCKRecord() {
        assert(true, "Not implemented.")
    }
}

extension iCloudSavable {

    func saveToICloud(type: iCloudType, completion: ((ckRecord: CKRecord?, error: NSError?) -> Void)) {
        let ckRecord = self.convertToCKRecord()
        let ckDatabase = ProtocolHelper.getCloudKitDatabase(type)
        ckDatabase.saveRecord(ckRecord) { (savedRecord, error) -> Void in
            completion(ckRecord: savedRecord, error:error)
        }
    }
}

extension iCloudDeletable {
    
    func deleteFromICloud(type: iCloudType, completion: ((ckRecord: CKRecordID?, error: NSError?) -> Void)) {
        let ckRecordID = CKRecordID(recordName: self.ID)
        let ckDatabase = ProtocolHelper.getCloudKitDatabase(type)
        ckDatabase.deleteRecordWithID(ckRecordID) { (deletedGuitar, error) -> Void in
            completion(ckRecord: deletedGuitar, error:error)
        }
    }
}

extension iCloudUpdateable {
    
    func updateICloud(type: iCloudType, completion: ((error: NSError?) -> Void)) {
        let updateRecordsOperation = CKModifyRecordsOperation()
        var guitarsCKRecordsArray = [CKRecord]()
        let ckRecord = self.convertToCKRecord()
        guitarsCKRecordsArray.append(ckRecord)
        
        updateRecordsOperation.recordsToSave = guitarsCKRecordsArray
        updateRecordsOperation.savePolicy = .AllKeys
        updateRecordsOperation.modifyRecordsCompletionBlock = { savedRecords, deletedRecordIDs, error in
            completion(error:error) // TODO: Unsmell me.
        }
        
        let ckDatabase = ProtocolHelper.getCloudKitDatabase(type)
        ckDatabase.addOperation(updateRecordsOperation)
    }
}

extension iCloudRecallable {
    
    // TODO: Refactor this to use self.convertFromCKRecord.
    func recallAllGuitarsFromICloud(type: iCloudType, completion: ((guitars: Array<Guitar>, error: NSError?) -> Void)) {
        var retval = Array<Guitar>()
        
        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "Guitar", predicate: predicate)
        let queryOperation = CKQueryOperation(query: query)
        
        queryOperation.recordFetchedBlock = { (fetchedGuitar) -> Void in
            retval.append(ProtocolHelper.convertCKRecordToGuitar(fetchedGuitar))
        }
        
        queryOperation.queryCompletionBlock = { (cursor, error) -> Void in
            completion(guitars:retval, error: error)
        }
        let ckDatabase = ProtocolHelper.getCloudKitDatabase(type)
        ckDatabase.addOperation(queryOperation)
    }
    
    // TODO: Refactor this to use self.convertFromCKRecord.
    func recallUserSettings(type: iCloudType, completion: ((userSettings: UserSetting?, error: NSError?) -> Void)) {
        var retval : UserSetting? = UserSetting()
        
        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "UserSetting", predicate: predicate)
        let queryOperation = CKQueryOperation(query: query)
        
        queryOperation.recordFetchedBlock = { (fetchedUserSetting) -> Void in
            retval = ProtocolHelper.convertCKRecordUserSetting(fetchedUserSetting)
        }
        
        queryOperation.queryCompletionBlock = { (cursor, error) -> Void in
            completion(userSettings:retval, error: error)
        }
        let ckDatabase = ProtocolHelper.getCloudKitDatabase(type)
        ckDatabase.addOperation(queryOperation)
    }
}

// MARK: - Extension helper

class ProtocolHelper {
    
    static func getCloudKitDatabase(type: iCloudType) -> CKDatabase {
        if type == iCloudType.Private {
            return CKContainer.defaultContainer().privateCloudDatabase
        } else {
            return CKContainer.defaultContainer().publicCloudDatabase
        }
    }
    
    // TODO: Delete this and use self.convertFromCKRecord instead.
    static func convertCKRecordToGuitar(ckrecord : CKRecord) -> Guitar {
        return Guitar(ID: ckrecord.recordID.recordName,
            model: ckrecord["Model"] as! String,
            lastChangedDate: ckrecord["LastChangedDate"] as! NSDate)
    }
    
    // TODO: Delete this and use self.convertFromCKRecord instead.
    static func convertCKRecordUserSetting(ckrecord : CKRecord) -> UserSetting {
        return UserSetting(ID: ckrecord.recordID.recordName, alertThreshold: ckrecord["AlertThresholdDays"] as! Int )
    }
}
