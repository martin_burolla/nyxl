//
//  AppDelegate.swift
//  nyxl
//
//  Created by Martin Burolla on 12/19/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        _ = SoundManager.sharedInstance()
        
        let appInitManager = AppInitializerManager()
        appInitManager.initializeAppIfFirstTime()
        
        return true
    }
    
    // MARK: - Background fetch
    
    func application(application: UIApplication, performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.NewData)
        
        let guitarNotificationManager = GuitarNotificationManager()
        guitarNotificationManager.showLocalNotificationForGuitarsThatNeedNewStrings()
    }
}
