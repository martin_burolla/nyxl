//
//  StringChangeManager.swift
//  nyxl
//
//  Created by Martin Burolla on 12/21/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import UIKit

// MARK: - GuitarManager

final class GuitarManager: NSObject, GuitarManagerProtocol {

    // MARK: - Data members
    
    let dataAccess = DataAccess()
    var delegate: GuitarManagerDelegate! = nil
    
    // MARK: - Construction
    
    override init() {
        super.init()
    }
    
    // MARK: - CRUD
    
    func fetchAllGuitars() {
        dataAccess.fetchAllGuitars { (guitars, error) -> Void in
            self.delegate?.didFetchAllGuitars(self, guitars: guitars, error: error)
        }
    }
    
    func addGuitar(newGuitar: GuitarViewModel) {
        let guitarDataModel = Guitar(guitarViewModel: newGuitar)
        dataAccess.addGuitar(guitarDataModel) { (ckRecord, error) -> Void in
             self.delegate?.didAddNewGuitar(self, guitar: newGuitar, error: error)
        }
    }
    
    func deleteGuitar(guitar: GuitarViewModel) {
        let guitarDataModel = Guitar(guitarViewModel: guitar)
        
        dataAccess.deleteGuitar(guitarDataModel) { (ckRecord, error) -> Void in
            self.delegate?.didDeleteGuitar(self, guitar: guitar, error: error)
        }
    }
    
    func updateDateForGuitar(guitar: GuitarViewModel, newDate: NSDate) {
        guitar.dateChanged = newDate
        let guitarDataModel = Guitar(guitarViewModel: guitar)
        
        dataAccess.updateStringDateForGuitar(guitarDataModel) { (guitar, error) -> Void in
            self.delegate?.didUpdateDateForGuitar(self, guitar: GuitarViewModel(guitarDataModel: guitar), error: error)
        }
    }
}
