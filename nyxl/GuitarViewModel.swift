//
//  Guitar.swift
//  nyxl
//
//  Created by Martin Burolla on 12/21/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import UIKit


final class GuitarViewModel : CustomStringConvertible {

    // MARK: - Data Members
    
    var ID = ""
    var model = ""
    var dateChanged = NSDate()
    var daysSinceLastChanged = 0
    var needsStringChange = false
    var description: String { return "(\(ID), \(model), \(dateChanged.toString()), \(daysSinceLastChanged))" }

    // MARK: - Construction
    
    init(model: String, lastChangedDate: NSDate) {
        self.model = model
        self.dateChanged = lastChangedDate
    }
    
    init(model: String, lastChangedDate: NSDate, ID: String) {
        self.model = model
        self.dateChanged = lastChangedDate
        self.ID = ID
    }
    
    init(guitarDataModel: Guitar) {
        self.model = guitarDataModel.Model
        self.ID = guitarDataModel.ID
        self.dateChanged = guitarDataModel.LastChangedDate
    }
    
    // MARK: - Formatting
    
    var formattedDateChanged: String {
        return "Last changed: " + dateChanged.toString()
    }
}
