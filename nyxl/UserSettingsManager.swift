//
//  UserSettingsManager.swift
//  nyxl
//
//  Created by Marty Burolla on 1/19/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

final class UserSettingsManager: NSObject, UserSettingsProtocol {

    // MARK: - Data members
    
    let dataAccess = DataAccess()
    var delegate: UserSettingsDelegate! = nil
    
    // MARK: - Cloud Settings
    
    func fetchUserSettings() {
        dataAccess.fetchUserSettings { (userSettings, error) -> Void in
            self.delegate?.didFetchUserSettings(self, userSettings: userSettings, error: error)
        }
    }
    
    func updateUserSettings(userSettings: UserSetting) {
        dataAccess.updateUserSettings(userSettings) { (error) -> Void in
            self.delegate?.didUpdateUserSettings(self, userSettings: userSettings, error: error)
        }
    }
    
    // MARK: - Local Settings
    
    func saveDateOfLastBackgroundFetch(date: NSDate) {
        return dataAccess.userDefaultsProxy.saveDateOfLastBackgroundFetch(date)
    }
    
    func getDateOfLastBackgroundFetch() -> NSDate {
        return dataAccess.getDateOfLastBackgroundFetch()
    }
}
