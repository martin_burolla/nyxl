//
//  SoundManager.swift
//  nyxl
//
//  Created by Martin Burolla on 12/19/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import Foundation
import AVFoundation

final class SoundManager : NSObject, SoundManagerProtocol {

    // MARK: - Data members
    
    static var instance: SoundManager!
    static var audioPlayers = [AVAudioPlayer]() // 6 players for each string.
    
    // MARK: - Singleton Construction

    class func sharedInstance() -> SoundManager {
        self.instance = (self.instance ?? SoundManager())
        
        for guitarString in GuitarStrings.allValues {
             audioPlayers.append(self.createAudioPlayerForGuitarString(guitarString))
        }

        return self.instance
    }
    
    // MARK: - Sound
    
    static func playSoundForString(theString: GuitarStrings) {
         self.audioPlayers[theString.hashValue].play()
    }
    
    // MARK: - Helpers
    
    static private func createAudioPlayerForGuitarString(theString: GuitarStrings) -> AVAudioPlayer {
        var retval = AVAudioPlayer()
        let stringMP3  = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(theString.description, ofType: "mp3")!)
        
        do {
            retval = try AVAudioPlayer(contentsOfURL: stringMP3, fileTypeHint: nil)
            retval.prepareToPlay()
        } catch {
            print(error)
        }
        
        return retval
    }
}
