//
//  UserSetting.swift
//  nyxl
//
//  Created by Martin Burolla on 1/23/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation

class UserSetting: NSObject,
            iCloudStorage,
            iCloudSavable,
            iCloudUpdateable,
            iCloudRecallable {
    
    // MARK: - Data Members
    
    var ID                 : String = ""
    var AlertThresholdDays : Int = 0
    
    // MARK: - Construction
    
    override init(){ }
    
    init (ID: String, alertThreshold: Int) {
        self.ID = ID
        self.AlertThresholdDays = alertThreshold
    }
}
