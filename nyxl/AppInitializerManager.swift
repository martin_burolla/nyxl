//
//  Initializer.swift
//  nyxl
//
//  Created by Martin Burolla on 1/23/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation

class AppInitializerManager: NSObject  {

    // MARK: - Data Members
    
    let dataAccess = DataAccess()
    let AlertThresholdDays: Int = 30
    
    // MARK: - Construction
    
    override init() {
        super.init()
    }
    
    // MARK: - App Initialization
    
    func initializeAppIfFirstTime() {
        if (dataAccess.getInitializationFlag() == nil) {
            GuitarNotificationManager.askUserForPermisionToShowLocalAlerts()
            GuitarNotificationManager.setupBackgroundFetchForDailyRefreshInterval()
            
            dataAccess.saveDateOfLastBackgroundFetch(NSDate(timeIntervalSince1970: 0.0))
            setupUserSettingsInICloud()
            dataAccess.saveInitializationFlag(true)
        }
    }
    
    private func setupUserSettingsInICloud() {
        let userSettings = UserSetting()
        userSettings.ID = NSUUID().UUIDString
        userSettings.AlertThresholdDays = AlertThresholdDays
        dataAccess.updateUserSettings(userSettings) { (error) -> Void in
            if error != nil {
                print(error)
            }
        }
    }
}
