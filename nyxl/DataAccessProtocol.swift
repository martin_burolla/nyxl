//
//  DataAccessProtocol.swift
//  nyxl
//
//  Created by Marty Burolla on 1/25/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation
import CloudKit

protocol DataAccessProtocol {

    // MARK: - Guitars 
    
    func addGuitar(guitar: Guitar, completion: ((ckRecord: CKRecord?, error: NSError?) -> Void))
    func deleteGuitar(guitar: Guitar, completion: ((ckRecord: CKRecordID?, error: NSError?) -> Void))
    func updateStringDateForGuitar(guitar: Guitar,completion: ((guitar: Guitar, error: NSError?) -> Void))
    func fetchAllGuitars(completion: ((guitars: Array<GuitarViewModel>, error: NSError?) -> Void))

    // MARK: - User Settings
    
    func fetchUserSettings(completion: (userSettings: UserSetting?, error: NSError?)-> Void)
    func updateUserSettings(userSetting: UserSetting, completion: (error: NSError?)-> Void)
    func saveDateOfLastBackgroundFetch(date: NSDate)
    func getDateOfLastBackgroundFetch() -> NSDate
    func saveInitializationFlag(value: Bool)
    func getInitializationFlag() -> Bool?
}