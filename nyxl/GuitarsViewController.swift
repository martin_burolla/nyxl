//
//  GuitarsViewController.swift
//  nyxl
//
//  Created by Martin Burolla on 12/21/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import UIKit

class GuitarsViewController : UIViewController,
                              UITableViewDelegate,
                              UITableViewDataSource,
                              GuitarManagerDelegate {

    // MARK: - Data members
    
    let navigationBar         = UINavigationBar()
    let tableView             = UITableView()
    let refreshControl        = UIRefreshControl()
    let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    let guitarManager         = GuitarManager()
    var guitars               = Array<GuitarViewModel>()
    let reuseIdentifier       = "GuitarTableViewCell"
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
       setupUI()
       setupAutolayout()
       setupManagers()
       fetchAllGuitars()
       
       activityIndicatorView.startAnimating()
    }
    
    // MARK: - Data

    func fetchAllGuitars() {
        guitarManager.fetchAllGuitars()
    }

    // MARK: - Callback GuitarManagerDelegate
    
    func didFetchAllGuitars(guitarManager: GuitarManagerProtocol, guitars: Array<GuitarViewModel>, error: NSError?) {
        self.guitars = guitars
        refreshControl.endRefreshing()
        activityIndicatorView.stopAnimating()
        tableView.reloadData()
    }
    
    func didAddNewGuitar(guitarManager: GuitarManagerProtocol, guitar: GuitarViewModel, error: NSError?) {
        guitars.append(guitar)
        tableView.reloadData()
    }
    
    func didDeleteGuitar(guitarManager: GuitarManagerProtocol, guitar: GuitarViewModel, error: NSError?) {
        guitars = self.guitars.filter() { $0.ID != guitar.ID }
        tableView.reloadData()
    }
    
    func didUpdateDateForGuitar(guitarManager: GuitarManagerProtocol, guitar: GuitarViewModel, error: NSError?) {
        let index = self.guitars.indexOf({ $0.ID == guitar.ID })
        guitars[index!].dateChanged = guitar.dateChanged
        guitars[index!].daysSinceLastChanged  = guitar.daysSinceLastChanged    
        tableView.reloadData()
    }
    
    // MARK: - User Interaction
    
    func addNewGuitar(sender:UIBarButtonItem!) {
        let addNewGuitarViewController = AddNewGuitarViewController(guitarManager: self.guitarManager)
        self.presentViewController(addNewGuitarViewController, animated: true) { () -> Void in }
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guitars.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return GuitarTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: reuseIdentifier, guitarViewModel: guitars[indexPath.row] )
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let refreshAlert = UIAlertController(title: "NYXL", message: "New strings for " + guitars[indexPath.row].model + "?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Confirm", style: .Default, handler: { (action: UIAlertAction!) in
            self.guitarManager.updateDateForGuitar(self.guitars[indexPath.row], newDate: NSDate())
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            refreshAlert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        guitarManager.deleteGuitar(self.guitars[indexPath.row]) // Swipe to delete.
    }
    
    // MARK: - Managers
    
    private func setupManagers() {
        guitarManager.delegate = self
    }
    
    // MARK: - UI
    
    private func setupUI() {
        view.backgroundColor = UIColor.whiteColor()

        // Navigation Tool Bar
        navigationBar.barStyle = UIBarStyle.Default
        navigationBar.translatesAutoresizingMaskIntoConstraints = false
        let navItem = UINavigationItem(title:"String Age")
        navItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .Add, target: self, action: "addNewGuitar:")
        self.navigationBar.pushNavigationItem(navItem, animated: false)
        self.view.addSubview(self.navigationBar)

        // Tableview
        tableView.registerClass(GuitarTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = NYXLStyleSheet.extraLightGrayColor
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.tableView)
        
        // RefreshControl
        refreshControl.addTarget(self, action: "fetchAllGuitars", forControlEvents: .ValueChanged)
        tableView.addSubview(self.refreshControl)
        
        // Activity indicator
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.backgroundColor = NYXLStyleSheet.extraLightGrayColor
        view.addSubview(self.activityIndicatorView)
    }
    
    private func setupAutolayout() {
        
        // Navigation Tool Bar
        self.navigationBar.topAnchor.constraintEqualToAnchor(self.view.topAnchor, constant: NYXLStyleSheet.statusBarHeight).active = true
        self.navigationBar.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor).active = true
        self.navigationBar.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor).active = true

        // Tableview
        self.tableView.topAnchor.constraintEqualToAnchor(self.navigationBar.bottomAnchor).active = true
        self.tableView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor).active = true
        self.tableView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor).active = true
        self.tableView.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor).active = true
        
        // Activity indicator
        self.activityIndicatorView.topAnchor.constraintEqualToAnchor(self.view.topAnchor).active = true;
        self.activityIndicatorView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor).active = true;
        self.activityIndicatorView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor).active = true;
        self.activityIndicatorView.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor).active = true;
    }
}


