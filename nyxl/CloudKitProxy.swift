//
//  CloudKitProxy.swift
//  Persistence
//
//  Created by Marty Burolla on 1/14/16.
//  Copyright © 2016 Marty Burolla. All rights reserved.
//

import UIKit
import CloudKit

// MARK: - CloudKitProxy

final class CloudKitProxy: NSObject {

    // MARK: - Cloud Mutations
    
    func addGuitar(guitar: Guitar, completion: ((ckRecord: CKRecord?, error: NSError?) -> Void)) {
        guitar.ID = NSUUID().UUIDString
        guitar.saveToICloud(.Private) { (ckRecord, error) -> Void in
            completion(ckRecord: ckRecord, error: error)
        }
    }
    
    func deleteGuitar(guitar: Guitar, completion: ((ckRecord: CKRecordID?, error: NSError?) -> Void)) {
        guitar.deleteFromICloud(.Private) { (ckRecord, error) -> Void in
            completion(ckRecord: ckRecord, error: error)
        }
    }
    
    func updateStringDateForGuitar(guitar: Guitar,completion: ((guitar: Guitar, error: NSError?) -> Void)) {
        guitar.updateICloud(.Private) { (error) -> Void in
            guitar.LastChangedDate = NSDate()
            completion(guitar: guitar, error: error)
        }
    }
    
    // MARK: - Cloud Retrieval
    
    func fetchAllGuitars(completion: ((guitars: Array<Guitar>, error: NSError?) -> Void)) {
        let guitar = Guitar(ID: "Hack")
        guitar.recallAllGuitarsFromICloud(.Private) { (guitars, error) -> Void in
            completion(guitars: guitars, error: error)
        }
    }
    
    // MARK: - User Settings
    
    func fetchUserSettings(completion: (userSettings: UserSetting?, error: NSError?)-> Void) {
        let userSetting = UserSetting()
        userSetting.recallUserSettings(.Private) { (userSettings, error) -> Void in
            completion(userSettings: userSettings, error: error)
        }
    }
    
    func updateUserSettings(userSetting: UserSetting, completion: (error: NSError?)-> Void) {
        userSetting.updateICloud(.Private) { (error) -> Void in
            completion(error: error)
        }
    }
}
