//
//  SettingsViewController.swift
//  nyxl
//
//  Created by Marty Burolla on 1/12/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController,
                    UIPickerViewDelegate,
                    UIPickerViewDataSource,
                    UserSettingsDelegate {

    // MARK: - Data members
    
    let pickerLabel         = UILabel()
    let uiPickerView        = UIPickerView()
    let userSettingsManager = UserSettingsManager()
    let daysArray           = AlertDaysViewModel.Days
    var userSettings        = UserSetting()
    
    // MARK : - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        self.setupAutoLayout()
        self.userSettingsManager.delegate = self
        self.userSettingsManager.fetchUserSettings()
    }
    
    // MARK: - UserSettingsDelegate
    
    func didFetchUserSettings(userSettingsManager: UserSettingsProtocol, userSettings: UserSetting?, error: NSError?) {
        self.userSettings = userSettings!
        self.uiPickerView.selectRow(self.daysArray.indexOf("\(self.userSettings.AlertThresholdDays)")!, inComponent: 0, animated: true)
    }
    
    func didUpdateUserSettings(userSettingsManager: UserSettingsProtocol, userSettings: UserSetting, error: NSError?) {
        //print("Alert threshold: \(userSettings.AlertThresholdDays)")
    }
    
    // MARK: - UIPickerView
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.userSettings.AlertThresholdDays = Int(self.daysArray[row])!
        self.userSettingsManager.updateUserSettings(self.userSettings)
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.daysArray[row]
    }
    
    // MARK: - UIPickerViewDataSource
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return daysArray.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // MARK: - UI
    
    func setupUI() {
        
        // Picker label
        self.pickerLabel.backgroundColor = UIColor.whiteColor()
        self.pickerLabel.text = "Notification Alert Theshold (Days)"
        self.pickerLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.pickerLabel)
      
        // UIPickerView
        self.uiPickerView.delegate = self
        self.uiPickerView.dataSource = self
        self.uiPickerView.backgroundColor = NYXLStyleSheet.extraLightGrayColor
        self.uiPickerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.uiPickerView)
    }
    
    func setupAutoLayout() {
        
        // Picker label
        self.pickerLabel.topAnchor.constraintEqualToAnchor(self.view.topAnchor, constant:  NYXLStyleSheet.statusBarHeight).active = true
        self.pickerLabel.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor).active = true
        self.pickerLabel.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor).active = true
        self.pickerLabel.heightAnchor.constraintEqualToConstant(30).active = true
        
        // UIPickerView
        self.uiPickerView.topAnchor.constraintEqualToAnchor(self.pickerLabel.bottomAnchor).active = true
        self.uiPickerView.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor).active = true
        self.uiPickerView.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor).active = true
        self.uiPickerView.heightAnchor.constraintEqualToConstant(NYXLStyleSheet.pickerViewHeight).active = true
    }
}
