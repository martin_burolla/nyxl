//
//  AddNewGuitarViewController.swift
//  nyxl
//
//  Created by Martin Burolla on 12/23/15.
//  Copyright © 2015 Martin Burolla. All rights reserved.
//

import UIKit

class AddNewGuitarViewController: UIViewController {

    // MARK: - Data members
    
    let textField       = UITextField()
    let addGuitarButton = UIButton(type: .System)
    var guitarManager: GuitarManager? = nil
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupAutoLayout()
    }
    
    // MARK: - Construction
    
    init(guitarManager: GuitarManager) {
        super.init(nibName: nil, bundle: nil)
        self.guitarManager = guitarManager
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - User Interaction
    
    func buttonAddGuitarTouched(sender:UIButton!) {
        if (self.textField.text?.characters.count >= 1) {
            let newGuitar = GuitarViewModel(model: self.textField.text!, lastChangedDate: NSDate())
            guitarManager!.addGuitar(newGuitar)
            
            textField.text = ""
            textField.resignFirstResponder()
            dismissViewControllerAnimated(true, completion: { })
        }
    }
    
    // MARK: - UI
    
    private func setupUI() {
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        // Textfield
        textField.backgroundColor = NYXLStyleSheet.extraLightGrayColor
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Guitar Name"
        view.addSubview(self.textField)

        // Button
        addGuitarButton.enabled = true
        addGuitarButton.backgroundColor = NYXLStyleSheet.extraLightGrayColor
        addGuitarButton.setTitle("Add New Guitar", forState: .Normal)
        addGuitarButton.addTarget(self, action: Selector("buttonAddGuitarTouched:"), forControlEvents: UIControlEvents.TouchUpInside)
        addGuitarButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.addGuitarButton)
    }

    private func setupAutoLayout() {
        
        // Textfield
        textField.topAnchor.constraintEqualToAnchor(self.view.topAnchor, constant: 40).active = true
        textField.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant:5).active = true
        textField.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -5).active = true
        textField.borderStyle = UITextBorderStyle.RoundedRect
        textField.heightAnchor.constraintEqualToConstant(50).active = true

        // Add Guitar Button
        addGuitarButton.topAnchor.constraintEqualToAnchor(self.textField.bottomAnchor, constant: 20).active = true
        addGuitarButton.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant:  5).active = true
        addGuitarButton.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -5).active = true
        addGuitarButton.heightAnchor.constraintEqualToConstant(NYXLStyleSheet.addGuitarButtonHeight).active = true
    }
}
