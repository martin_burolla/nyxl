# Guitar String Manager

A simple universal iOS app that helps guitar players manage their guitars that use D'Addario NYXL strings.

<img src=https://github.com/mburolla/ReadMePix/blob/master/nyxl/nyxl_home.png width=210 height=400 >
<img src=https://github.com/mburolla/ReadMePix/blob/master/nyxl/nyxl_guitars.png width=210 height=400 >
<img src=https://github.com/mburolla/ReadMePix/blob/master/nyxl/nyxl_purchase.png width=210 height=400 >
<img src=https://github.com/mburolla/ReadMePix/blob/master/nyxl/nyxlnotification.PNG width=210 height=400 >

# Features

* Provides reference tones and colors to assist string changes
* Stores guitars and string age data in your iCloud account
* Background fetch sends local UI notifications for guitars that need new strings
* Select Local notification alert threshold via UIPickerView
* Easily reorder new guitar strings from Amazon.com

# Architecture

This app uses the [Questions](https://github.com/mburolla/CoffeeApp) architecture style.  This style of coding clearly identifies the user interface, application logic and data without question.

<img src=https://github.com/mburolla/ReadMePix/blob/master/nyxl/Questions.png width=899 height=388>

# Miscellaneous

* Builds with Xcode 7.2
* CloudKit
* Swift 2.0
* CocoaPods
* CI/CD Master branch: 
[![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=56d0d171a715c4010008a851&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/56d0d171a715c4010008a851/build/latest)
