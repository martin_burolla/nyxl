//
//  DataAccess.swift
//  nyxl
//
//  Created by Marty Burolla on 1/25/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation
import CloudKit

final class DataAccess: NSObject, DataAccessProtocol {

    // MARK: - Data Sources
    
    let cloudKitProxy     = CloudKitProxy()
    let userDefaultsProxy = UserDefaultsProxy()
    
    // MARK: - Cloud Mutations
    
    func addGuitar(guitar: Guitar, completion: ((ckRecord: CKRecord?, error: NSError?) -> Void)) {
        
        // TODO: Cache switching.
        
        cloudKitProxy.addGuitar(guitar) { (ckRecord, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                completion(ckRecord: ckRecord, error: error)
            })
        }
    }
    
    func deleteGuitar(guitar: Guitar, completion: ((ckRecord: CKRecordID?, error: NSError?) -> Void)) {
        
        // TODO: Cache switching.
        
        cloudKitProxy.deleteGuitar(guitar) { (ckRecord, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                completion(ckRecord: ckRecord, error: error)
            })
        }
    }
    
    func updateStringDateForGuitar(guitar: Guitar,completion: ((guitar: Guitar, error: NSError?) -> Void)) {
        
        // TODO: Cache switching.
        
        cloudKitProxy.updateStringDateForGuitar(guitar) { (guitar, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                completion(guitar: guitar, error: error)
            })
        }
    }
    
    // MARK: - Cloud Retrieval
    
    func fetchAllGuitars(completion: ((guitars: Array<GuitarViewModel>, error: NSError?) -> Void)) {
        
        // TODO: Cache switching.
        
        cloudKitProxy.fetchUserSettings { (userSettings, error) -> Void in
            self.cloudKitProxy.fetchAllGuitars { (guitars, error) -> Void in
                var guitarViewModels = Array<GuitarViewModel>()
                
                for guitar in guitars {
                    let guitarViewModel = GuitarViewModel(guitarDataModel: guitar)
                    guitarViewModel.daysSinceLastChanged = NSDate().daysFrom(guitar.LastChangedDate)
                    
                    if (guitarViewModel.daysSinceLastChanged > userSettings!.AlertThresholdDays) {
                        guitarViewModel.needsStringChange = true
                    }
                    guitarViewModels.append(guitarViewModel)
                }
                
                let sortedGuitars = guitarViewModels.sort { $0.daysSinceLastChanged > $1.daysSinceLastChanged }
                
                dispatch_async(dispatch_get_main_queue(), {
                    completion(guitars: sortedGuitars, error: error)
                })
            }
        }
    }
    
    // MARK: - Cloud User Settings
    
    func fetchUserSettings(completion: (userSettings: UserSetting?, error: NSError?)-> Void) {
        
        // TODO: Cache switching.
        
        cloudKitProxy.fetchUserSettings { (userSettings, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                completion(userSettings: userSettings, error: error)
            })
        }
    }
    
    func updateUserSettings(userSetting: UserSetting, completion: (error: NSError?)-> Void) {
        cloudKitProxy.updateUserSettings(userSetting) { (error) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                completion(error: error)
            })
        }
    }
    
    // MARK: - Local User Settings
    
    func saveDateOfLastBackgroundFetch(date: NSDate) {
        userDefaultsProxy.saveDateOfLastBackgroundFetch(date)
    }
    
    func getDateOfLastBackgroundFetch() -> NSDate {
        return userDefaultsProxy.getDateOfLastBackgroundFetch()
    }
    
    // MARK: - Initialization
    
    func saveInitializationFlag(value: Bool) {
        self.userDefaultsProxy.saveInitializationFlag(value)
    }
    
    func getInitializationFlag() -> Bool? {
        return self.userDefaultsProxy.getInitializationFlag()
    }
}
