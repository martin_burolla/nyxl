//: Playground - noun: a place where people can play

import Cocoa


protocol Testable {
    
    
}

extension Testable  {
    
    func Props() -> Array<String>{
        return Mirror(reflecting: self).children.filter { $0.label != nil }.map { $0.label! }
        
    }
}


class Person : Testable {
    var firstname : String = "Marty"
    //var lastname : String = "Burolla"
    
    func tet(){
      
    }
}

let p = Person()
p.firstname

p



print(p.firstname)

